#include <stdio.h>

#define MSG "Wailord\n"

double dbl(double goku);
void arrayTest();
void testInput();
int palindrome();
void testStructs();
int sommeAges();

typedef struct Personne {
    char *prenom;
    int age;
    float taille;
} Person;
struct Famille {
    char *nomF;
    struct Personne pere;
    struct Personne mere;
    int nbEnfants;
    struct Personne enfants[5];
//y a une limite d'enfants par famille, c'est parce que C n'aimait pas les arrays flexibles (+ on est surpeuplés)
};

int main() {
    testStructs();
}

void testStructs() {
    struct Famille f1;
    printf("size of : %i\n", (int)sizeof(f1));
    Person p1 = { .prenom = "Pierre", .age = 57, .taille = 1.82};
    Person p2 = { .prenom = "Marie", .age = 48, .taille = 1.67};
    Person p3 = { .prenom = "Jean", .age = 11, .taille = 1.56};
    Person p4 = { .prenom = "Georges", .age = 14, .taille = 1.79};
    f1.nomF = "Hubert";
    f1.pere = p1;
    f1.mere = p2;
    f1.nbEnfants = 2;
    f1.enfants[0] = p3;
    f1.enfants[1] = p4;
    printf("somme des ages : %i\n", sommeAges(&f1));
}
int sommeAges(struct Famille *fP) {
    int sum = fP->pere.age + fP->mere.age;
    for (int i = 0; i < fP->nbEnfants; i++)
        sum += fP->enfants[i].age;
    return sum;
}
int palindrome(char *s) {
    int length;
    char *noe = &s[0];
    for (length = 0; *noe != '\0' && *noe != '\n'; length++, noe++); //calcul de s.length par incrémentation
    for (int i = 0; i < length/2; i++) {
        if (s[i] != s[length-i-1]) return 0;
    }
    return 1;
}
void testInput() {
    char chaine[10];
    printf("Entrez une chaîne: ");
    fgets(chaine, 10, stdin);
    //fputs(chaine, stdout);
    printf("chaîne : %s\n", chaine);

    int length;
    char *noe = &chaine[0];
    for (length = 0; *noe != '\0' && *noe != '\n'; length++, noe++) printf("%i\n", length);
    for (int i = 0; i < 10; i++) printf("%c\n", chaine[i]);
    printf("longueur : %i\n", length);
    printf("vraie longueur : %li\n", sizeof(chaine));
}
double dbl(double goku) {
    return 2*goku;
}
void arrayTest() {
    double arr[3] = {0};
    printf("arr : %p\n", arr);
    printf("&arr[0] : %p\n", &arr[0]);
    printf("&arr[1] : %p\n", &arr[1]);
    printf("arr[2] : %f\n", arr[2]);
    printf("arr[3] : %f\n", arr[3]);
}